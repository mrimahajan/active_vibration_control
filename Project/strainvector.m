function [ strain ] = strainvector(w,length,z,t )
% w is function of x and time
% strain only in x direction needs to be calculated 
% t is thickness of sensor 
% z is z coordinate of lower surface of sensor i.e at z dist above if z +ve or below
% mid plane of beam if z -ve
% length is x dimension of beam

nx =size(w,2);          %no of positions in x (>=3)
nt =size(w,1);          %no of time values(>=2 for villary sensor)

for i=1:100     %thickness divided into 100 parts as strain varies with z coordinate
        for j=1:nt
            strain(i,j,:)=-(z+t/100*i)*diff(diff(w(j,:)))/(length/nx)^2;    %no.of columns reduced by 2 
        end
end

end
