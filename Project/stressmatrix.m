function [ stress ] = stressmatrix(strain,E1,E2,v,Gr)

%E1,E2,Gr(GPa),v are material properties


nt =size(strain,2);           % no. of time positons at which w-displacement recorded
nx = size(strain,3);          % no.0f x positions - 2

delta = 1-v^2;
E1n = E1*10^9;
E2n = E2*10^9;
Grn = Gr*10^9;


 Q=[E1n/delta,v*E2n/delta,0;v*E2n/delta,E2n/delta,0;0,0,Grn];        %compliance matrix
 
 
    
for i=1:100              %assume there are 100 divisions along thickness
    for j = 1:nt
        for k=1:nx
            stress(i,j,k,:)= Q(:,1)*strain(i,j,k);     %since strain only in x direction is nonzero
        end
    end
end
   
end
