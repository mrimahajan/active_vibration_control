function [ Voltage ] = IPMC(w,K,length,interval)
%w is dispalcement in z direction as function of x and time
% K is experimental constant

nx =size(w,2);          %no of positions in x (>=3)
nt =size(w,1);          %no of time values



for i=1:nt
    Voltage(i,:)=diff(diff(w(i,:)))/(length/nx)^2;      %dimension in x reduced by 2
end

Voltage = Voltage + ones(nt,nx-2)*.05;

i=1:nt;
subplot(2,1,1)
plot((i-1)*interval,(Voltage(:,nx-2))','r');          %voltage at third last position
title('Voltage vs time')
i=1:nx-2;
subplot(2,1,2)
plot(i*length/nx,Voltage(nt,:),'r');                  %voltage at end pt. of time
title('Voltage vs x')

end
