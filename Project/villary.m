function [ Voltage ] = villary(w,length,width,z,t,E1,E2,v,Gr,d,interval,N)
% w is function of x and time
% strain only in x direction needs to be calculated 
% t is thickness of sensor 
% z is z coordinate of lower surface of sensor i.e at z dist above if z +ve or below
% mid plane of beam if z -ve
% length,width and t are x,y and z dimensions of beam respectively
%E1,E2,v,Gr,e33 are material properties
%d =[d31,d32,d33] in 10^-9 m/A is part of magnetostrictive coeff matrix used for
%calculating B
%interval is time interval at which voltage is measured
%N is no. of coils in sensor

strain = strainvector(w,length,z,t);
stress = stressmatrix(strain,E1,E2,v,Gr);

nx = size(stress,3);              % 2 less than actual no. of x positions
nt = size(stress,2);              % no. of time pts. at which w is recorded
ldiff = length/(nx+2);
X = ldiff:ldiff:length-2*ldiff;      %values of x at which sress was obtained



%calculating B as a function of x,z and time
                 
    for i=1:100      %dividing sensor into 100 parts across thickness
    for j=1:nt
        for k=1:nx
            
            B(k,i,j)=0;
            for l=1:3
                B(k,i,j)= B(k,i,j)+d(1,l)*(10^(-9))*stress(i,j,k,l);
            end
        end
    end
    end
%calculating flux as function of z and time
    
    for i=1:100
    for j=1:nt
        fi(i,j)= width*trapz(X,B(:,i,j));      %integrating BdA for x-y surface @ each z 
    end
    end
    
    netflux = zeros(1,nt);
Z = t/100:t/100:t;

for i=1:nt
    netflux(i)=trapz(Z,fi(:,i));              %calculating netflux by integration over entire thickness
end

Voltage = -N*diff(netflux)/interval;          %calculating voltage as function of time(diff reduce dimension t by 1)

i=1:nt-1;
plot((i-1)*interval,Voltage,'r');              %plot of voltage vs time
    

end
