function [ Voltage ] = piezosensing(w,length,width,z,t,E1,E2,v,Gr,e33,d,interval)
% w is function of x and time
% strain only in x direction needs to be calculated 
% t is thickness of sensor 
% z is z coordinate of lower surface of sensor i.e at z dist above if z positive  or below
% mid plane of beam if z -ve
% length,width and t are x,y and z dimensions of sensor beam respectively
%E1,E2,v,Gr,e33 are material properties
%d =[d31,d32,d33] (in 10^-12 m/V) part of peizoelec. coeff matrix used for electric
%dispacement calculation
%interval is time interval at which voltage is measured

strain = strainvector(w,length,z,t);
stress = stressmatrix(strain,E1,E2,v,Gr);

capacitance =e33*length*width/t;

nx = size(stress,3);             %no of x positions -2
nt = size(stress,2);             %no.of time at equal interval at which data observed
ldiff = length/(nx+2);
X = ldiff:ldiff:length-2*ldiff;      %stress was obtained for this range of x

%calculating electrical displacement as function of x,z and time

for i=1:100      %assume thickness divided into 100 diff x-y planes
    for j=1:nt
        for k=1:nx
            
            D(k,i,j)=0;
            for l=1:3
                D(k,i,j)= D(k,i,j)+d(1,l)*(10^(-12))*stress(i,j,k,l);
            end
        end
    end
end

%calculating charge q @ each z

for i=1:100
    for j=1:nt
        Q(i,j)= width*trapz(X,D(:,i,j));      %integrating electric displacement over x-y plane
    end
end

netcharge = zeros(1,nt);
Z = t/100:t/100:t;

%calculating net charge generated

for i=1:nt
    netcharge(i)=trapz(Z,Q(:,i));                  %integrating charge over thickness
    Voltage(i)=netcharge(i)/capacitance;
end

i=1:nt;
plot((i-1)*interval,Voltage,'r');                  %plotting voltage vs time

end
